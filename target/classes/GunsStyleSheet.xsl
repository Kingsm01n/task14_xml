
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <body>
                <h2>My Guns</h2>
                <table border = "1">
                    <tr bgcolor = "Blue">
                        <th>Model</th>
                        <th>Handy</th>
                        <th>Origin</th>
                        <th>Distance Of Fire</th>
                        <th>Sighting Distance</th>
                        <th>Score</th>
                        <th>Optic</th>
                    </tr>
                    <xsl:for-each select="Guns/Model">
                        <tr bgcolor="Green">
                            <td><xsl:value-of select="Name"/></td>
                            <td><xsl:value-of select="Handy"/></td>
                            <td><xsl:value-of select="Origin"/></td>
                            <td><xsl:value-of select="TTC/DistanceOfFire"/></td>
                            <td><xsl:value-of select="TTC/SightingDistance"/></td>
                            <td><xsl:value-of select="TTC/Score"/></td>
                            <td><xsl:value-of select="TTC/Optic"/></td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>