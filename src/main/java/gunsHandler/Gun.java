package gunsHandler;

import java.util.ArrayList;
import java.util.Collection;

public class Gun{
    int handy, fireDistance, sightingDistance;
    String scope, optic, origin, model;

    public int getHandy() {
        return handy;
    }

    public void setHandy(int handy) {
        this.handy = handy;
    }

    public int getFireDistance() {
        return fireDistance;
    }

    public void setFireDistance(int fireDistance) {
        this.fireDistance = fireDistance;
    }

    public int getSightingDistance() {
        return sightingDistance;
    }

    public void setSightingDistance(int sightingDistance) {
        this.sightingDistance = sightingDistance;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getOptic() {
        return optic;
    }

    public void setOptic(String optic) {
        this.optic = optic;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}