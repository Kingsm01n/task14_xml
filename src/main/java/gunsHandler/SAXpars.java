package gunsHandler;

import MVCpattern.View;
import gunsHandler.Gun;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class SAXpars extends DefaultHandler {
    Gun gun = new Gun();
    String thisElement = "";

    public Gun getGun(){
        return gun;
    }

    @Override
    public void startDocument() throws SAXException{
        View.display("Start parser XML...");
    }

    @Override
    public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException{
        thisElement = qName;
    }

    @Override
    public void endElement(String namespaceURI, String localName, String qName){
        thisElement = "";
    }

    @Override
    public void characters(char[] ch, int start, int length){
        if(thisElement.equals("Name")){
            gun.setModel(new String(ch, start, length));
        }
        if(thisElement.equals("Handy")){
            gun.setHandy(new Integer(new String(ch, start, length)));
        }
        if(thisElement.equals("Origin")){
            gun.setOrigin(new String(ch, start, length));
        }
        if(thisElement.equals("DistanceOfFire")){
            gun.setFireDistance(new Integer(new String(ch, start, length)));
        }
        if(thisElement.equals("SightingDistance")){
            gun.setSightingDistance(new Integer(new String(ch, start, length)));
        }
        if(thisElement.equals("Score")){
            gun.setScope(new String(ch, start, length));
        }
        if(thisElement.equals("Optic")){
            gun.setOptic(new String(ch, start, length));
        }
    }
}
