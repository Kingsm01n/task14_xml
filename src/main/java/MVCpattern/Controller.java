package MVCpattern;

import gunsHandler.Gun;
import gunsHandler.SAXpars;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class Controller {
    ArrayList<Gun> guns = new ArrayList<Gun>();
    SAXParserFactory factory = SAXParserFactory.newInstance();
    SAXParser parser;
    {
        try {
            parser = factory.newSAXParser();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
    }
    SAXpars saxp = new SAXpars();

    public void setArrayList() throws SAXException, IOException {
        parser.parse(new File("C:\\Users\\demid\\IdeaProjects\\task14\\src\\XML\\Guns.xml"), saxp);
        guns.add(saxp.getGun());
        View.display(guns.get(0).getOrigin());
    }
}
